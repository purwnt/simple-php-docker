# Simple PHP 7.0 Apache with Docker

## Build image
docker build -t php-test .

## Run image
docker run -p 80:80 php-test

## Run image during development
docker run -p 80:80 -v /home/datanest/Documents/learn/docker/app1/src/:/var/www/html php-test

